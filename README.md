#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>                               


//函数外全局变量定义
int position_x,position_y;  //飞机位置
int bullet_x,bullet_y;  //子弹位置
int high,width;    //游戏画面
int enemy_x,enemy_y;  //尺寸敌机位置
int score;          //游戏得分

void gotoxy(int x,int y)  //光标移动到（x,y）位置
{
    HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X=x;
    pos.Y=y;
    SetConsoleCursorPosition(handle,pos);
}

void HideCursor()
{
    CONSOLE_CURSOR_INFO cursor_info={1,0};//第二个值为0表示隐藏光标
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info);
}

void startup()
{
    high=18;
    width=30;

    position_x=high/2;
    position_y=width/2;

    bullet_x=-1;
    bullet_y=position_y;

    enemy_x=0;
    enemy_y=width/2;

    score=0;
}

void show()   //显示画面
{
    int i,j;
    gotoxy(0,0);//光标移动到原点位置，一下重画清屏
    for(i=0;i<high;i++)
    {
        for(j=0;j<width;j++)
        {
            if((i==position_x)&&(j==position_y))
                printf("*");  //输入飞机*
            else if((i==bullet_x)&&(j==bullet_y))
                printf("|");  //输入子弹
            else if((i==enemy_x)&&(j==enemy_y))
                printf("@");  //输入敌机@
            else
                printf(" ");  //输入空格
        }
        printf("\n");
    }
    printf("得分:%d\n",score);
}

void    updateWithoutInput()   //与用户输入无关的更新
{    static int speed = 0;//用于控制敌人下落速度
    if ((bullet_x == enemy_x) && (bullet_y == enemy_y))
    {
        score++;
        speed++;
        enemy_x = 0;
        enemy_y = rand() % width;
        bullet_x = -1;
    
    }
    if (bullet_x > -1)
        bullet_x--;//子弹向上运动
        
        

    if (speed < 10)
        speed++;

    if (enemy_x > high)
    {
        enemy_x = 0;
        enemy_y = rand() % width;
    }
    else
    {
        if (speed == 10)
        {
            enemy_x++;
            speed = 0;
        }
    }

}


void  updateWithInput()     //与用户输入无关的更新
{
    char input;
    if (kbhit())//当按键时执行
    {
        input = getch();
        if (input == 'w'&&position_x>0 )
            position_x--;
        if (input == 's'&&position_x<high)
            position_x++;
        
        if (input == 'a'&&position_y>0)
            position_y--;
        if (input == 'd'&&position_y<width)
            position_y++;
        if (input = ' ')
        {
            bullet_x = position_x - 1;
            bullet_y = position_y;
        }
        
    }
    
}


int main()
{
    startup();                  //数据初始化
    while(1)                    //游戏循环执行

    {
        show();                 //显示画面
        updateWithoutInput();   //与用户输入无关的更新
        updateWithInput();      //与用户输入无关的更新
    }
    return 0;
}